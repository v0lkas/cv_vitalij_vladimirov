FROM ubuntu:18.04

ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

# PHP 5.6
RUN apt-get update && apt-get install -y software-properties-common
RUN apt-get install -y php-pear
RUN apt-add-repository ppa:ondrej/php && apt-get purge -y software-properties-common && apt-get update
RUN apt-get install -y nginx-light \
        php5.6-fpm \
        php5.6-cli \
        php5.6-common \
        php5.6-json \
        php5.6-opcache \
        php5.6-mysql \
        php5.6-gd \
        php5.6-curl \
        php5.6-bcmath \
        php5.6-mbstring \
        php5.6-mcrypt \
        php5.6-xml \
        mc \
        supervisor \
  && apt-get clean
RUN mkdir -p /run/php
RUN sed -i 's/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/' /etc/php/5.6/fpm/php.ini
RUN echo 'fastcgi_read_timeout 7200s;' >> /etc/nginx/fastcgi_params

# Nginx
RUN { echo 'daemon off;'; cat /etc/nginx/nginx.conf; } > /tmp/nginx.conf && mv /tmp/nginx.conf /etc/nginx/nginx.conf
COPY docker_conf/nginx/nginx-site.conf /etc/nginx/sites-available/default
COPY docker_conf/nginx/nginx.conf /etc/nginx/nginx.conf
COPY docker_conf/supervisord/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
EXPOSE 80

CMD ["/usr/bin/supervisord"]
