# Vitalij Vladimirov CV source code
This is the simple example of CV creation with PHP + HTML/CSS + JavaScript, running project with Docker.

## Main info
| Info | Description |
| --- | --- |
| Container IP | 192.168.99.100 |
| Container port | 80 |
| PHP version | 5.6 (can be changed to PHP 7.2 in Dockerfile) |

## Useful commands
| Command | Description |
| --- | --- |
| ./run.sh -start | Run container with its own network |
| ./run.sh -restart | Reboot container |
| ./run.sh -stop | Stop container |
| ./run.sh -login | Login to container |