function loadingScript()
{
    setTimeout(function() { checkSizes(); },250);
}

function checkSizes()
{
    var menuHeight = document.getElementById('menu').offsetHeight;
    var infoHeight = document.getElementById('info').offsetHeight;

    if (menuHeight > infoHeight) {
        document.getElementById('info').style.height = menuHeight - 50 + 'px';
    } else {
        document.getElementById('menu').style.height = infoHeight - 55 + 'px';
    }
}

function mailer(name,domain,ending)
{
    window.location = 'mailto:'+name+'@'+domain+'.'+ending;
}

function percentage(prcnt)
{
    document.write('<div class="percentage" title="'+prcnt+'%"><span style="width:'+prcnt+'%;"><\/span><\/div>');
}

function buttonAction(action)
{
    var languages = ['lt','en'];

    if (action == 'print') {
        window.print();
    } else if (action == 'download') {
        window.location.href = '/download/' + document.documentElement.lang;
    } else if (languages.indexOf(action) > -1) {
        changeLanguage(action);
    }

    return false;
}

function changeLanguage(lang)
{
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            document.open();
            document.write(this.responseText);
            document.close();

            window.history.pushState(lang, lang, '/' + lang);

            return false;
        }
    };
    xhttp.open("GET", "/"+lang, true);
    xhttp.send();
}

var contactVitalij = String.fromCharCode(60,97,32,104,114,101,102,61,34,106,97,118,97,115,99,114,105,112,116,
    58,109,97,105,108,101,114,40,39,118,105,116,97,108,105,106,39,44,39,118,108,97,100,105,109,105,114,111,118,39,
    44,39,108,116,39,41,59,34,62,118,105,116,97,108,105,106,38,35,54,52,59,118,108,97,100,105,109,105,114,111,118,
    46,108,116,60,47,97,62);

var contactJevgenij = String.fromCharCode(60,97,32,104,114,101,102,61,34,106,97,118,97,115,99,114,105,112,116,
    58,109,97,105,108,101,114,40,39,106,101,118,103,101,110,105,106,39,44,39,118,105,108,110,105,117,115,99,111,100,
    105,110,103,39,44,39,108,116,39,41,59,34,62,106,101,118,103,101,110,105,106,38,35,54,52,59,118,105,108,110,105,
    117,115,99,111,100,105,110,103,46,108,116,60,47,97,62);