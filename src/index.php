<?php
$files = scandir('./includes');
foreach ($files as $file) {
    $explodeFile = explode(".", $file);
    $ext = $explodeFile[count($explodeFile)-1];
    if ($ext == 'php') {
        require_once './includes/' . $file;
    }
}

$counter = new Counter();

$page = explode('/', $_SERVER['REQUEST_URI']);

if (!isset($page[1]) || empty($page[1]) || file_exists('./langs/' . $page[1] . '.json')) {

    if (isset($page[1]) && file_exists('./langs/' . $page[1] . '.json')) {
        $template = new Template($page[1]);
    } else {
        $template = new Template();
    }

    if (isset($page[1]) && isset($page[2]) && $page[2] == 'content') {
        $template->setContent();
    } else {
        $counter->setLog($_SERVER, 'website');
        $template->setHtml();
    }

} elseif (

    isset($page[1]) && $page[1] == 'viewLog'
    && file_exists('./includes/IpList.php') && (new IpList())->getAction() == 'doNotTrack'

) {
    $counter->getLog();
} elseif ($page[1] == 'download') {

    $counter->setLog($_SERVER, 'download');
    $file = './cv/CV_Vitalij_Vladimirov_' . strtoupper($page[2]) . '.pdf';
    (new Files())->download($file);

} else {

    (new Redirect())->run('linkedin');

}