<?php
class Files
{
    /**
     * @param string $file
     */
    public function download($file)
    {
        if (file_exists($file)) {
            header("Content-Description: File Transfer");
            header("Content-Type: application/pdf");
            header('Content-Length: ' . filesize($file));
            header("Content-Disposition: attachment; filename='" . basename($file) . "'");
            readfile($file);
        } else {
            echo 'File not found';
        }

        exit;
    }
}