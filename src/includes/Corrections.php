<?php
class Corrections
{
    /**
     * @param string $text
     * @param int $lineLength
     * @param int $tabs
     * @return string
     */
    public function lineSplitter($text,$lineLength = 80,$tabs = 1)
    {
        $newText = '';
        $line = '';

        $split = explode(' ',$text);
        foreach ($split as $word) {
            if (strlen($line . $word) > $lineLength) {
                $newText .= $line;
                $line = "\n";

                if ($tabs >= 1) {
                    for ($i = 1; $i <= $tabs; ++$i) {
                        $line .= "\t";
                    }
                }
            }
            $line .= " " . $word;
        }
        $newText .= $line;

        return $newText;
    }
}