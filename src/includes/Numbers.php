<?php
class Numbers
{
    /**
     * @param DateTime $birthday
     * @return int
     * @throws Exception
     */
    public function getAge($birthday)
    {
        $date1 = new DateTime($birthday);
        $date2 = new DateTime();
        $age = $date1->diff($date2)->y;

        return $age;
    }
}