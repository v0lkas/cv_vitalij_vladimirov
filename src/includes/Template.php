<?php
class Template
{
    private $lang;
    private $text;

    /**
     * Template constructor.
     * @param string $lang
     */
    public function __construct($lang = 'lt')
    {
        $this->lang = $lang;
        $this->text = $this->getLangFile();
    }

    /**
     * @return array
     */
    private function getLangFile()
    {
        $json = file_get_contents('./langs/' . $this->lang . '.json');
        return json_decode($json, true);
    }

    public function setHtml()
    {
        echo '<!DOCTYPE HTML>
<html lang="' . $this->lang . '">
<head>
	<title>' . $this->text['manual']['title'] . '</title>
    <meta charset="utf-8">
    <meta name="author" content="' . $this->text['manual']['name'] . '">
	<meta name="robots" content="NOINDEX,NOFOLLOW,NOARCHIVE">
	<link rel="shortcut icon" href="/images/favicon.ico">
	<link media="screen,print" href="/styles.web.css" rel="stylesheet">
	<link media="print" href="/styles.print.css" rel="stylesheet">
	<script src="/scripts.js"></script>
</head>
<body onload="loadingScript();">
<div id="content">
' . $this->setMenu() . '
' . $this->setInfo() . '
</div>
</body>
</html>';
    }

    public function setContent()
    {
        echo $this->setMenu();
        echo $this->setInfo();
    }

    /**
     * @return string
     */
    private function setMenu()
    {
        $manual = $this->text['manual'];
        $skills = $this->text['skills'];

        $menu = "\t" . '<div id="menu">
		<img id="photo" src="/images/' . str_replace(' ','',$manual['name']) . '.jpeg" alt="">
		<h2>' . $manual['contacts']['title'] . '</h2>';

        foreach ($manual['contacts'] as $name => $value) {
            if ($name != 'title') {
                $menu .= "\n\t\t". '<div class="iconed_text">';

                if (file_exists('./images/' . $name . '.png')) {
                    $menu .= "\n\t\t\t" . '<img alt="" src="/images/' . $name . '.png">';
                } else {
                    $menu .= "\n\t\t\t" . '<img alt="" src="/images/spacer.png">';
                }

                $menu .= "\n\t\t\t" . '<div class="answer';

                if (strpos($value, '<br>') !== false) {
                    $menu .= ' two-lines';
                }

                $menu .= '">' . $value . '</div>' . "\n\t\t" . '</div>';
            }
        }

        foreach ($skills as $skillBlock) {
            $menu .= "\n\t\t". '<h2>' . $skillBlock['title'] . '</h2>';

            foreach ($skillBlock['values'] as $skill) {
                $menu .= "\n\t\t" . '<p class="prcnt">' .
                    "\n\t\t\t" . '<strong>' . $skill['title'] . '</strong>' .
                    "\n\t\t\t" . '<span class="prcnt_txt">' . $skill['value'] . '</span>' .
                    "\n\t\t\t" . '<script>percentage(' . $skill['percentage'] . ');</script>' .
                    "\n\t\t" . '</p>';
            }
        }

        $menu .= "\n\t" . '</div>';

        return $menu;
    }

    /**
     * @return string
     * @throws Exception
     */
    private function setInfo()
    {
        $summary = "\t" . '<div id="info">';

        $summary .= $this->setIcons();
        $summary .= $this->setHeader();
        $summary .= $this->setSummary();

        $summary .= "\n\t" . '</div>';

        return $summary;
    }

    /**
     * @return string
     */
    private function setIcons()
    {
        $buttons = $this->text['manual']['buttons'];

        $icons = "\n\t\t" . '<div id="langs">';

        foreach ($buttons as $name => $value) {
            if (file_exists('./images/' . $name . '.png')) {
                $icons .= "\n\t\t\t" . '<a href="';
                    if ($name == 'download') {
                        $icons .= '/' . $name . '/' . $this->lang;
                    } elseif ($name == 'print') {
                        $icons .= '#';
                    } else {
                        $icons .= '/' . $name;
                    }
                $icons .= '" onclick="return buttonAction(\'' . $name . '\')">' .
                    '<img alt="' . $value . '" title="' . $value . '" id="ico_' . $name . '"' .
                    ' onmouseover="this.style.opacity=\'1.0\';" onmouseout="this.style.opacity=\'0.5\';"' .
                    ' src="/images/' . $name . '.png"></a>';
            }
        }

        $icons .= "\n\t\t" . '</div>';


        return $icons;
    }

    /**
     * @return string
     * @throws Exception
     */
    private function setHeader()
    {
        $manual = $this->text['manual'];
        $correct = new Corrections();

        $age = (new Numbers())->getAge($manual['birthdate']['value']);

        $header = "\n\t\t" . '<h1>' . $manual['name'] . '</h1>' .
            "\n\t\t" . '<div>' .
            "\n\t\t\t" . '<span class="first">' . $manual['profession']['title'] . '</span>' .
            "\n\t\t\t" . '<span class="second">' . $manual['profession']['value'] . '</span>' .
            "\n\t\t" . '</div>' . "\n\t\t" . '<div>' .
            "\n\t\t\t" . '<span class="first">' . $manual['birthdate']['title'] . '</span>' .
            "\n\t\t\t" . '<span class="second">' . $manual['birthdate']['value'] .
                ' <span class="help" title="' . $age . ' ' . $manual['birthdate']['years'] . '">(' . $age . ')</span></span>' .
            "\n\t\t" . '</div>';

        if (isset($manual['introduction'][0])) {
            $header .= "\n\t\t" . '<div class="second">';

            foreach ($manual['introduction'] as $text) {
                $header .= "\n\t\t\t" . '<p>' . "\n\t\t\t\t" . $correct->lineSplitter($text, 100, 4) . "\n\t\t\t" . '</p>';
            }

            $header .= "\n\t\t" . '</div>';
        }

        return $header;
    }

    /**
     * @return string
     */
    private function setSummary()
    {
        $summary = '';

        foreach ($this->text['summary'] as $data) {
            $summary .= "\n\t\t" . '<h2>' . $data['title'] . '</h2>' .
                "\n\t\t" . '<div class="skills">';

                foreach ($data['values'] as $value) {
                    if (isset($value['period']) || isset($value['title']) || isset($value['period'])) {
                        $summary .= "\n\t\t\t" . '<h3>';

                        if (isset($value['period'])) {
                            $summary .= "\n\t\t\t\t" . '<span class="date">' . $value['period'] . '</span>' . "\n\t\t\t\t";
                        }

                        if (isset($value['title'])) {
                            if (isset($value['url'])) {
                                $summary .= '<a target="_blank" href="' . $value['url'] . '">' . $value['title'] . '</a>';
                            } else {
                                $summary .= $value['title'];
                            }

                            if (isset($value['title2'])) {
                                if (substr($value['title2'],0,1) != ',') {
                                    $summary .= ' ';
                                }
                                $summary .= '<span class="extra">' . $value['title2'] . '</span>';
                            }
                        }

                        if (isset($value['period'])) {
                            $summary .= "\n\t\t\t";
                        }
                        $summary .= '</h3>';
                    }

                    if (isset($value['name']) && isset($value['url'])) {

                        $summary .= "\n\t\t\t" . '<div>' .
                                "\n\t\t\t\t" . '<span class="first">' . $value['name'] . '</span>' .
                                "\n\t\t\t\t" . '<span class="second"><a target="_blank" href="' . $value['url'] . '">' . $value['url'] . '</a></span>' .
		                    "\n\t\t\t" . '</div>';
                    }

                    if (isset($value['text'])) {

                        $summary .= "\n\t\t\t" . '<p>' . $value['text'];

                        if (isset($value['text2'])) {
                            $summary .= ' (' . $value['text2'] . ')';
                        }

                        $summary .= '</p>';
                    }

                    if (isset($value['list'])) {
                        $summary .= "\n\t\t\t" . '<ul>';
                        foreach ($value['list'] as $list) {
                            $summary .= "\n\t\t\t\t" . '<li>' . $list . '</li>';
                        }
                        $summary .= "\n\t\t\t" . '</ul>';
                    }
                }

            $summary .= "\n\t\t" . '</div>';
        }

        return $summary;
    }
}