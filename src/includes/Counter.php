<?php
class Counter
{
    private $action = 'show';

    public function __construct()
    {
        if (file_exists('./includes/IpList.php')) {
            $this->action = (new IpList())->getAction();
        }

        if ($this->action == 'redirect') {
            $this->setLog($_SERVER,  'redirect');
            (new Redirect())->run('linkedin');
        }
    }

    /**
     * @param array $data
     * @param string $type
     * @throws Exception
     */
    public function setLog($data, $type = 'website')
    {
        if ($this->action != 'doNotTrack') {

            $str = (new DateTime())->format("Y-m-d H:i:s");
            $str .= "\t" . $type;
            $str .= "\t" . $data['REQUEST_URI'];
            $str .= "\t" . $data['REMOTE_ADDR'];
            $str .= "\t" . $data['HTTP_USER_AGENT'];

            if (isset($data['HTTP_REFERER']) && !empty($data['HTTP_REFERER'])) {
                $str .= "\t" . $data['HTTP_REFERER'];
            }

            $str .= "\n";

            file_put_contents('./logCounter.txt', $str, FILE_APPEND | LOCK_EX);
        }
    }

    public function getLog()
    {
        if (file_exists('./logCounter.txt')) {
            $log = file('./logCounter.txt');

            echo '<table cellspacing="0" cellpadding="3" border="1">
<tr>
    <th>Time</th>
    <th>Type</th>
    <th>Url</th>
    <th>IP</th>
    <th>Agent</th>
    <th>Referer</th>
</tr>';

            for ($i = count($log)-1; $i >= 0; $i--) {
                $line = str_replace(["\n","\r"],"",$log[$i]);
                list($time,$type,$url,$ip,$agent,$referer) = explode("\t",$line);

                echo "<tr>
    <td>$time</td>
    <td>$type</td>
    <td>$url</td>
    <td>$ip</td>
    <td>$agent</td>
    <td>$referer</td>
</tr>";
            }

            echo "\n</table>";
        }
    }
}