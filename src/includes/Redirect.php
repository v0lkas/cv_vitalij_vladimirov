<?php
class Redirect
{
    private $list = [
        'linkedin' => 'https://www.linkedin.com/in/vitalij-vladimirov/;'
    ];

    /**
     * @param string $item
     */
    public function run($item)
    {
        if (isset($this->list[$item])) {
            header ('location: ' . $this->list[$item]);
        }

        exit;
    }
}