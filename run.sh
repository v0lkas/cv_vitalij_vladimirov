#!/bin/bash

URL=cv_vitalij_vladimirov
PORT=80
NET=cv_net
SUBNET=192.168.98.0
IP=192.168.98.100
DIR="$(cd "$(dirname "$z")"; pwd)/$(basename "$z")src"

if [ $# -eq 0 ];
    then
        echo ""
        echo "No command entered:"
        echo "  -start            start $URL docker"
        echo "  -restart          restart $URL docker"
        echo "  -stop             stop $URL docker"
        echo "  -login            login $URL container"

elif [ $1 = '-test' ];
    then
        echo "### testing bash"
        echo $DIR

elif [ $1 = "-start" ];
    then
        echo "# starting docker image $URL"
        docker network create --driver=bridge --subnet=$SUBNET/24 $NET
        docker build -t $URL .
        docker run -d -p $PORT:80 -v $DIR:/var/www --network=$NET --ip="$IP" --name $URL $URL

elif [ $1 = "-restart" ];
    then
        echo "# restarting docker image $URL"
        docker stop $URL
        docker rm $URL
        docker build -t $URL .
        docker run -d -p $PORT:80 -v $DIR:/var/www --network=$NET --ip="$IP" --name $URL $URL

elif [ $1 = "-stop" ];
    then
        echo "# stopping docker image $URL"
        docker stop $URL
        docker rm $URL
        docker network rm $NET

elif [ $1 = "-login" ];
    then
        echo "# login $URL container"
        docker exec -it $URL bash

fi